<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'auth'], function () {
    Route::post('login', ['uses' => 'Auth\TokenAuthController@authenticate']);

    Route::post('reset_password/email', 'Auth\ResetPasswordController@postEmail');
    Route::post('reset_password', 'Auth\ResetPasswordController@postReset');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['token.auth'])->group(function () {

    Route::group(['prefix' => 'general'], function () {

        Route::get('cities', ['uses' => 'General\CityController@getList']);

    });

    Route::group(['prefix' => 'admin', 'middleware' => 'role:admin'], function () {
        Route::post('users', ['uses' => 'Admin\User\UserController@createUser']);
    });

    Route::group(['prefix' => 'profile', 'middleware' => 'role:user'], function () {

        Route::get('documents', ['uses' => 'User\Profile\UserDocumentController@get']);
        Route::get('username', ['uses' => 'User\Profile\ProfileController@checkUsername']);
        Route::get('', ['uses' => 'User\Profile\ProfileController@getInfo']);

        Route::post('security', ['uses' => 'User\Profile\SecurityController@edit']);
        Route::post('documents', ['uses' => 'User\Profile\UserDocumentController@edit']);
        Route::post('avatar', ['uses' => 'User\Profile\AvatarController@uploadAvatar']);
        Route::post('', ['uses' => 'User\Profile\ProfileController@editInfo']);
    });

    Route::group(['prefix' => 'organizations', 'middleware' => 'role:user'], function () {

        Route::post('users', ['uses' => 'User\Organization\UserController@createUser']);

    });

    Route::group(['prefix' => 'projects', 'middleware' => 'role:user'], function () {

        Route::get('', ['uses' => 'User\Project\ProjectController@getUserProjects']);
        Route::post('', ['uses' => 'User\Project\ProjectController@createProject']);

        Route::get('{project_id}/users', ['uses' => 'User\Project\UserController@getUsers']);
        Route::post('{project_id}/users', ['uses' => 'User\Project\UserController@addUserToProject']);
        Route::delete('{project_id}/users/{user_id}', ['uses' => 'User\Project\UserController@removeUserFromProject']);

    });

    Route::group(['prefix' => 'departments', 'middleware' => 'role:user'], function () {

        Route::get('', ['uses' => 'User\Department\DepartmentController@getUserDepartments']);
        Route::post('', ['uses' => 'User\Department\DepartmentController@create']);

        Route::get('{department_id}/users', ['uses' => 'User\Department\UserController@getUsers']);
        Route::post('{department_id}/users', ['uses' => 'User\Department\UserController@addUserToDepartment']);
        Route::delete('{department_id}/users/{user_id}', ['uses' => 'User\Department\UserController@removeUserFromDepartment']);

    });

});

Route::get('bad_request', function () {
    return sdfl;
});