<?php

namespace Tests\Unit\Services\User;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Hash;

use App\Services\ProjectService;
use App\Services\UserService;
use App\Services\DepartmentService;
use App\Services\ProjectUserService;

class ProjectServiceTest extends TestCase
{
    use DatabaseTransactions;

    public function test_success_create_project()
    {
        $projectUserService = app(ProjectUserService::class);
        $userService = app(UserService::class);
        $projectService = app(ProjectService::class);

        $userId = $userService->addOrganizationHeadUser([
            'last_name' => 'Иванов',
            'first_name' => 'Иван',
            'middle_name' => 'Иванович',
            'email' => 'ivanov@gmail.com',
            'birthday' => '1990-06-25',
            'phone' => '78005553535',
            'password' => Hash::make('secret')
        ], [
            'name' => 'Организация'
        ]);

        $userId2 = $userService->addOrganizationEmployeeUser([
            'last_name' => 'Петров',
            'first_name' => 'Петр',
            'middle_name' => 'Петрович',
            'email' => 'petrov@gmail.com',
            'birthday' => '1990-06-25',
            'phone' => '78005553537',
            'password' => Hash::make('secret')
        ], $userId);

        $projectId = $projectService->addProject(['name' => 'Test'], $userId);

        $project = $projectService->find($projectId);

        $this->assertEquals($project->name, 'Test');

        $this->assertTrue($projectService->isHeadProjectUser($projectId, $userId));
    }

    public function test_error_create_project()
    {
        $userId = app(UserService::class)->addOrganizationHeadUser([
            'last_name' => 'Иванов',
            'first_name' => 'Иван',
            'middle_name' => 'Иванович',
            'email' => 'ivanov@gmail.com',
            'birthday' => '1990-06-25',
            'phone' => '78005553535',
            'password' => Hash::make('secret')
        ], [
            'name' => 'Организация'
        ]);

        $projectId = app(ProjectService::class)->addProject(['wrong_param' => 'Test'], $userId);

        $this->assertFalse($projectId);
    }

    public function test_error_create_project_2()
    {
        $userId = app(UserService::class)->add([
            'last_name' => 'Иванов',
            'first_name' => 'Иван',
            'middle_name' => 'Иванович',
            'email' => 'ivanov@gmail.com',
            'birthday' => '1990-06-25',
            'phone' => '78005553535',
            'password' => Hash::make('secret')
        ]);

        $projectId = app(ProjectService::class)->addProject(['name' => 'Test'], $userId);

        $this->assertFalse($projectId);
    }
}