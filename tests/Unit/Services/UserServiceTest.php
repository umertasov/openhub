<?php

namespace Tests\Unit\Services\User;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Hash;
use Exception;

use App\Services\UserService;
use App\Services\UserDocumentService;
use App\Services\OrganizationService;
use App\Models\User;

class UserServiceTest extends TestCase
{
    use DatabaseTransactions;

    public function test_success_user_edit() : void
    {
        $userService = app(UserService::class);

        $userId = $userService->add([
            'last_name' => 'Иванов',
            'first_name' => 'Иван',
            'middle_name' => 'Иванович',
            'email' => 'ivanov@gmail.com',
            'birthday' => '1990-06-25',
            'phone' => '78005553535',
            'password' => Hash::make('secret')
        ]);

        $this->assertEquals(1, $userService->edit($userId, [
            'last_name' => 'Петров',
            'first_name' => 'Петр',
            'middle_name' => 'Петрович',
            'email' => 'petrov@gmail.com',
            'birthday' => '1995-06-25',
            'phone' => '78005553590',
            'city_id' => 1,
            'worktime_from' => '10:00',
            'worktime_to' => '20:00'
        ]));

        $user = $userService->find($userId);

        $this->assertEquals($user->last_name, 'Петров');
        $this->assertEquals($user->first_name, 'Петр');
        $this->assertEquals($user->middle_name, 'Петрович');
        $this->assertEquals($user->email, 'petrov@gmail.com');
        $this->assertEquals($user->birthday, '1995-06-25 00:00:00');
        $this->assertEquals($user->phone, '78005553590');
        $this->assertEquals($user->city_id, 1);
        $this->assertEquals($user->worktime_from, '10:00:00');
        $this->assertEquals($user->worktime_to, '20:00:00');
    }

    public function test_username_is_unique() : void
    {
        $user1 = factory(User::class)->create();
        $user2 = factory(User::class)->create();

        // check own username
        $this->assertTrue(app(UserService::class)->usernameIsUnique($user1->username, $user1->id));

        // username unique
        $this->assertTrue(app(UserService::class)->usernameIsUnique($user2->username . str_random(10), $user1->id));

        // username is not unique
        $this->assertFalse(app(UserService::class)->usernameIsUnique($user2->username, $user1->id));
    }

    public function test_successful_create_user() : void
    {
        $userService = app(UserService::class);
        $userDocumentService = app(UserDocumentService::class);

        $userId = $userService->add([
            'last_name' => 'Иванов',
            'first_name' => 'Иван',
            'middle_name' => 'Иванович',
            'email' => 'ivanov@gmail.com',
            'birthday' => '1990-06-25',
            'phone' => '78005553535',
            'password' => Hash::make('secret')
        ]);

        $this->assertInternalType('int', $userId);

        $user = $userService->find($userId);

        $this->assertInternalType('object', $user);

        $this->assertEquals('user' . $userId, $user->username);

        $documentsInfo = $userDocumentService->getByUserId($userId);
        $this->assertNotNull($documentsInfo);
    }

    public function test_error_create_user() : void
    {
        $userService = app(UserService::class);

        $userId = $userService->add([
            'last_name' => 'Иванов',
            'first_name' => 'Иван',
            'middle_name' => 'Иванович',
            'email' => 'ivanov@gmail.com',
            'birthday' => '1990-06-25',
            'phone' => '78005553535',
            'password' => Hash::make('secret'),
            'wrong_attr' => 1
        ]);

        $this->assertFalse($userId);
    }

    public function test_successful_create_head_user() : void
    {
        $userService = app(UserService::class);
        $userDocumentService = app(UserDocumentService::class);

        $userId = $userService->addOrganizationHeadUser([
            'last_name' => 'Иванов',
            'first_name' => 'Иван',
            'middle_name' => 'Иванович',
            'email' => 'ivanov@gmail.com',
            'birthday' => '1990-06-25',
            'phone' => '78005553535',
            'password' => Hash::make('secret')
        ], [
            'name' => 'Организация'
        ]);

        $this->assertInternalType('int', $userId);

        $user = $userService->find($userId);

        $this->assertInternalType('object', $user);

        $this->assertEquals('user' . $userId, $user->username);

        $documentsInfo = $userDocumentService->getByUserId($userId);
        $this->assertNotNull($documentsInfo);

        $organization = app(OrganizationService::class)->getFirstOrganizationUser($userId);
        $this->assertEquals($organization->name, 'Организация');
    }

    public function test_error_create_head_user() : void
    {
        $userService = app(UserService::class);

        $userId = $userService->addOrganizationHeadUser([
            'last_name' => 'Иванов',
            'first_name' => 'Иван',
            'middle_name' => 'Иванович',
            'email' => 'ivanov@gmail.com',
            'birthday' => '1990-06-25',
            'phone' => '78005553535',
            'password' => Hash::make('secret'),
            'wrong_attr' => 1
        ], [
            'name' => 'Организация'
        ]);

        $this->assertFalse($userId);
    }

    public function test_successful_create_employee_user() : void
    {
        $userService = app(UserService::class);
        $userDocumentService = app(UserDocumentService::class);

        $userHeadId = $userService->addOrganizationHeadUser([
            'last_name' => 'Иванов',
            'first_name' => 'Иван',
            'middle_name' => 'Иванович',
            'email' => 'ivanov@gmail.com',
            'birthday' => '1990-06-25',
            'phone' => '78005553535',
            'password' => Hash::make('secret')
        ], [
            'name' => 'Организация'
        ]);

        $organization = app(OrganizationService::class)->getFirstOrganizationUser($userHeadId);

        $userEmployeeId = $userService->addOrganizationEmployeeUser([
            'last_name' => 'Иванов',
            'first_name' => 'Иван',
            'middle_name' => 'Иванович',
            'email' => 'ivanov2@gmail.com',
            'birthday' => '1990-06-25',
            'phone' => '78005553536',
            'password' => Hash::make('secret')
        ], $userHeadId);

        $this->assertInternalType('int', $userEmployeeId);

        $user = $userService->find($userEmployeeId);

        $this->assertInternalType('object', $user);

        $this->assertEquals('user' . $userEmployeeId, $user->username);

        $documentsInfo = $userDocumentService->getByUserId($userEmployeeId);
        $this->assertNotNull($documentsInfo);

        $checkRole = app(OrganizationService::class)->checkUserRole($organization->id, $userEmployeeId, config('role.employee'));
        $this->assertTrue($checkRole);

        $checkRole = app(OrganizationService::class)->checkUserRole($organization->id, $userEmployeeId, config('role.head'));
        $this->assertFalse($checkRole);
    }

    public function test_error_create_employee_user() : void
    {
        $userService = app(UserService::class);

        $userHeadId = $userService->addOrganizationHeadUser([
            'last_name' => 'Иванов',
            'first_name' => 'Иван',
            'middle_name' => 'Иванович',
            'email' => 'ivanov@gmail.com',
            'birthday' => '1990-06-25',
            'phone' => '78005553535',
            'password' => Hash::make('secret')
        ], [
            'name' => 'Организация'
        ]);

        $userId = $userService->addOrganizationEmployeeUser([
            'last_name' => 'Иванов',
            'first_name' => 'Иван',
            'middle_name' => 'Иванович',
            'email' => 'ivanov@gmail.com',
            'birthday' => '1990-06-25',
            'phone' => '78005553535',
            'password' => Hash::make('secret'),
            'wrong_attr' => 1
        ], $userHeadId);

        $this->assertFalse($userId);
    }

    public function test_success_security_change() : void
    {
        $userService = app(UserService::class);

        $user = factory(User::class)->create();

        $this->assertTrue($userService->securityChange([
            'password' => 'password',
            'old_password' => 'secret',
            'email' => 'test@email.com',
            'old_email' => $user->email,
            'phone' => '79312345678',
            'old_phone' => $user->phone
        ], $user->id));

        $updatedUser = $userService->find($user->id);

        $this->assertTrue(Hash::check('password', $updatedUser->password));
        $this->assertEquals($updatedUser->email, 'test@email.com');
        $this->assertEquals($updatedUser->phone, '79312345678');
    }

    /**
     * @expectedException Exception
     */
    public function test_error_password_security_change() : void
    {
        $userService = app(UserService::class);

        $user = factory(User::class)->create();

        $this->assertInstanceOf(Exception::class, $userService->securityChange([
            'password' => 'password',
            'old_password' => 'wrong_password'
        ], $user->id));
    }

    /**
     * @expectedException Exception
     */
    public function test_error_email_security_change() : void
    {
        $userService = app(UserService::class);

        $user = factory(User::class)->create();

        $this->assertInstanceOf(Exception::class, $userService->securityChange([
            'email' => 'test@email.com',
            'old_email' => 'wrong_email'
        ], $user->id));
    }

    /**
     * @expectedException Exception
     */
    public function test_error_phone_security_change() : void
    {
        $userService = app(UserService::class);

        $user = factory(User::class)->create();

        $this->assertInstanceOf(Exception::class, $userService->securityChange([
            'phone' => '79312345678',
            'old_phone' => 'wrong_phone'
        ], $user->id));
    }

    public function test_user_has_role(): void
    {
        $userService = app(UserService::class);

        $userId = $userService->add([
            'last_name' => 'Иванов',
            'first_name' => 'Иван',
            'middle_name' => 'Иванович',
            'email' => 'ivanov@gmail.com',
            'birthday' => '1990-06-25',
            'phone' => '78005553535',
            'password' => Hash::make('secret')
        ]);

        $this->assertTrue($userService->hasRole($userId, 2));
        $this->assertFalse($userService->hasRole($userId, 1));
    }

}
