<?php

namespace Tests\Unit\Services\User;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Hash;

use App\Services\UserService;
use App\Services\DepartmentService;
use App\Services\DepartmentUserService;
use App\Services\ProjectService;

class DepartmentServiceTest extends TestCase
{
    use DatabaseTransactions;

    public function test_success_create_department()
    {
        $departmentUserService = app(DepartmentUserService::class);
        $userService = app(UserService::class);
        $projectService = app(ProjectService::class);
        $departmentService = app(DepartmentService::class);

        $userId = $userService->addOrganizationHeadUser([
            'last_name' => 'Иванов',
            'first_name' => 'Иван',
            'middle_name' => 'Иванович',
            'email' => 'ivanov@gmail.com',
            'birthday' => '1990-06-25',
            'phone' => '78005553535',
            'password' => Hash::make('secret')
        ], [
            'name' => 'Организация'
        ]);

        $userId2 = $userService->addOrganizationEmployeeUser([
            'last_name' => 'Петров',
            'first_name' => 'Петр',
            'middle_name' => 'Петрович',
            'email' => 'petrov@gmail.com',
            'birthday' => '1990-06-25',
            'phone' => '78005553537',
            'password' => Hash::make('secret')
        ], $userId);

        $projectId = $projectService->addProject(['name' => 'Test'], $userId);

        $project = $projectService->find($projectId);

        $this->assertEquals($project->name, 'Test');

        $this->assertTrue($projectService->isHeadProjectUser($projectId, $userId));

        $departmentId = $departmentService->addDepartment([
            'project_id' => $projectId,
            'name' => 'Отдел 1'
        ], $userId);

        $this->assertTrue($departmentService->isHeadDepartmentUser($departmentId, $userId));

        $department = $departmentService->find($departmentId);

        $this->assertEquals($department->project_id, $projectId);
        $this->assertEquals($department->name, 'Отдел 1');

        $departmentUserId = $departmentService->addUserToDepartment($departmentId, $userId2, config('role.employee'));

        $this->assertInternalType('int', $departmentUserId);

        $users = $departmentService->getDepartmentUsers($departmentId);
        $this->assertCount(2, $users);

        $departmentService->removeUserFromDepartment($departmentId, $userId2);
        $users = $departmentService->getDepartmentUsers($departmentId);
        $this->assertCount(1, $users);

        $departments = $departmentService->getUserDepartments($userId);
        $this->assertCount(1, $departments);
    }

    public function test_error_create_departments()
    {
        $userId = app(UserService::class)->addOrganizationHeadUser([
            'last_name' => 'Иванов',
            'first_name' => 'Иван',
            'middle_name' => 'Иванович',
            'email' => 'ivanov@gmail.com',
            'birthday' => '1990-06-25',
            'phone' => '78005553535',
            'password' => Hash::make('secret')
        ], [
            'name' => 'Организация'
        ]);

        $departmentId = app(DepartmentService::class)->addDepartment(['wrong_param' => 'Test'], $userId);

        $this->assertFalse($departmentId);
    }
}