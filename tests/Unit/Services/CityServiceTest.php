<?php

namespace Tests\Unit\Services\User;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Services\CityService;

class CityServiceTest extends TestCase
{
    use DatabaseTransactions;

    public function test_get_list_cities()
    {
        $cities = app(CityService::class)->getList();

        $this->assertInternalType('object', $cities);
    }
}