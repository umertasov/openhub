<?php

namespace Tests\Unit\Services\User;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Hash;

use App\Services\OrganizationService;
use App\Services\UserService;

class OrganizationServiceTest extends TestCase
{
    use DatabaseTransactions;

    public function test_error_get_first_organization()
    {
        $userId = app(UserService::class)->add([
            'last_name' => 'Иванов',
            'first_name' => 'Иван',
            'middle_name' => 'Иванович',
            'email' => 'ivanov@gmail.com',
            'birthday' => '1990-06-25',
            'phone' => '78005553535',
            'password' => Hash::make('secret')
        ]);

        $organizationService = app(OrganizationService::class);
        $this->assertFalse($organizationService->getFirstOrganizationUser($userId));
        $this->assertFalse($organizationService->getFirstOrganizationsIdByUser($userId));
    }
}