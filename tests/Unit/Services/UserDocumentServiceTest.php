<?php

namespace Tests\Unit\Services\User;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Hash;

use App\Services\UserService;
use App\Services\UserDocumentService;
use App\Models\User;

class UserDocumentServiceTest extends TestCase
{
    use DatabaseTransactions;

    public function test_get_empty_documents_info()
    {
        $user = factory(User::class)->create();

        $documentsInfo = app(UserDocumentService::class)->getByUserId($user->id);

        $this->assertInternalType('object', $documentsInfo);

        $this->assertEquals($documentsInfo->passport_series, '');
        $this->assertEquals($documentsInfo->passport_number, '');
        $this->assertEquals($documentsInfo->passport_place, '');
        $this->assertEquals($documentsInfo->inn, '');
    }

    public function test_success_edit_documents_info()
    {
        $userId = app(UserService::class)->add([
            'last_name' => 'Иванов',
            'first_name' => 'Иван',
            'middle_name' => 'Иванович',
            'email' => 'ivanov@gmail.com',
            'birthday' => '1990-06-25',
            'phone' => '78005553535',
            'password' => Hash::make('secret'),
        ]);

        $this->assertTrue(app(UserDocumentService::class)->editUserDocumentsInfo($userId, [
            'passport_series' => '5210',
            'passport_number' => '123456',
            'passport_place' => 'г Москва',
            'passport_date' => '2016-08-12',
            'inn' => '123456789',
            'snils' => '123123123'
        ]));
    }

    public function test_error_edit_documents_info()
    {
        $userId = app(UserService::class)->add([
            'last_name' => 'Иванов',
            'first_name' => 'Иван',
            'middle_name' => 'Иванович',
            'email' => 'ivanov@gmail.com',
            'birthday' => '1990-06-25',
            'phone' => '78005553535',
            'password' => Hash::make('secret'),
        ]);

        $this->assertFalse(app(UserDocumentService::class)->editUserDocumentsInfo($userId, [
            'passport_series' => '5210',
            'passport_number' => '123456',
            'passport_place' => 'г Москва',
            'passport_date' => '2016-08-12',
            'inn' => '123456789',
            'snils' => '123123123',
            'wrong_parameter' => '123'
        ]));
    }
}