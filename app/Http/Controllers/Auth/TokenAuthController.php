<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

/**
 * @codeCoverageIgnore
 */
class TokenAuthController extends Controller
{
    public function authenticate(Request $request)
    {
        // grab credentials from the request
        $credentials = $request->only('email', 'password');

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return jsonValidationError([
                    'message' => 'Неправильный email или пароль'
                ]);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return jsonServerError();
        }

        //$token = JWTAuth::getToken();
        $user = JWTAuth::setToken($token)->toUser();

        // all good so return the token
        return jsonSuccess(['token' => $token, 'user' => $user]);
    }
}