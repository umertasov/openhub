<?php

namespace App\Http\Controllers\General;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Services\CityService;

/**
 * @codeCoverageIgnore
 */
class CityController extends Controller
{
    protected $cityService;

    public function __construct(CityService $cityService)
    {
        $this->setAuthUser();
        $this->cityService = $cityService;
    }

    public function getList()
    {
        $cities = $this->cityService->getList();
        return jsonSuccess($cities);
    }
}