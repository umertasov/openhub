<?php

namespace App\Http\Controllers\User\Profile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Services\UserDocumentService;

/**
 * @codeCoverageIgnore
 */
class UserDocumentController extends Controller
{
    protected $userDocumentService;

    public function __construct(UserDocumentService $userDocumentService)
    {
        $this->setAuthUser();
        $this->userDocumentService = $userDocumentService;
    }

    /**
     * Get current user documents info
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function get()
    {
        return jsonSuccess($this->userDocumentService->getByUserId($this->authUser->id));
    }

    /**
     * Edit user documents info
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(Request $request)
    {
        $request->validate([
            'passport_series' => 'nullable|string',
            'passport_number' => 'nullable|string',
            'passport_place' => 'nullable|string',
            'passport_date' => 'nullable|date',
            'inn' => 'nullable|string',
            'snils' => 'nullable|string'
        ]);

        $documentPaste = $request->only(['passport_series', 'passport_number', 'passport_place', 'passport_date', 'inn', 'snils']);

        if (isset($documentPaste['passport_date'])) {
            $documentPaste['passport_date'] = dateToTimestamp($documentPaste['passport_date']);
        }

        if ($this->userDocumentService->editUserDocumentsInfo($this->authUser->id, $documentPaste)) {
            return jsonSuccess();
        }
        return jsonServerError();
    }
}