<?php

namespace App\Http\Controllers\User\Profile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Services\UserService;
use App\Traits\FileTrait;

/**
 * @codeCoverageIgnore
 */
class AvatarController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->setAuthUser();
        $this->userService = $userService;
    }

    public function uploadAvatar(Request $request)
    {
        $request->validate(['file' => 'required|file']);

        $done = $this->userService->uploadAvatar($request->file('file'), $this->authUser->id);

        if (!empty($done)) {
            return jsonSuccess();
        }
        return jsonServerError();
    }
}