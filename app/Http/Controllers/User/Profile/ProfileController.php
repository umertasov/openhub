<?php

namespace App\Http\Controllers\User\Profile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Services\UserService;

/**
 * @codeCoverageIgnore
 */
class ProfileController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->setAuthUser();
        $this->userService = $userService;
    }

    /**
     * Get main user info
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getInfo()
    {
        return jsonSuccess($this->authUser);
    }

    /**
     * Проверка username на уникальность
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkUsername(Request $request)
    {
        $request->validate(['username' => 'required|string|min:1']);

        if ($this->userService->usernameIsUnique($request->input('username'), $this->authUser->id)) {
            return jsonSuccess();
        }
        return jsonValidationError(['message' => 'username занят']);
    }

    /**
     * Редактирование основной информации профиля
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function editInfo(Request $request)
    {
        $request->validate([
            'last_name' => 'required|string|min:1',
            'first_name' => 'required|string|min:1',
            'middle_name' => 'nullable|string',
            'birthday' => 'required|date',
            'worktime_from' => 'nullable|string|regex:/^[0-9]{2}:[0-9]{2}$/u',
            'worktime_to' => 'nullable|string|regex:/^[0-9]{2}:[0-9]{2}$/u',
            'city_id' => 'nullable|integer|exists:cities,id'
        ]);

        try {
            $editUser = $request->only(['last_name', 'first_name', 'middle_name', 'birthday', 'worktime_from', 'worktime_to', 'city_id']);

            if (isset($editUser['birthday'])) {
                $editUser['birthday'] = dateToTimestamp($editUser['birthday']);
            }

            if (!isset($editUser['middle_name']) || empty($editUser['middle_name'])) {
                $editUser['middle_name'] = '';
            }

            $this->userService->edit($this->authUser->id, $editUser);

            return jsonSuccess();
        } catch (\Exception $e) {
            return jsonServerError();
        }
    }
}