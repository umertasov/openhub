<?php

namespace App\Http\Controllers\User\Profile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Services\UserService;

/**
 * @codeCoverageIgnore
 */
class SecurityController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->setAuthUser();
        $this->userService = $userService;
    }

    /**
     * Edit security options
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(Request $request)
    {
        $request->validate([
            'old_password' => 'nullable|string|required_with:password',
            'password' => 'nullable|confirmed|min:8',
            'old_email' => 'nullable|email|required_with:email',
            'email' => 'nullable|email',
            'old_phone' => 'nullable|string|required_with:phone',
            'phone' => 'nullable|string'
        ]);

        $values = $request->only(['old_password', 'password', 'old_email', 'email', 'old_phone', 'phone']);

        try {
            $this->userService->securityChange($values, $this->authUser->id);
            return jsonSuccess();
        } catch (\Exception $e) {
            return jsonValidationError(['message' => $e->getMessage()]);
        }
    }
}