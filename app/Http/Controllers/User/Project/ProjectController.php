<?php

namespace App\Http\Controllers\User\Project;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Services\ProjectService;

/**
 * @codeCoverageIgnore
 */
class ProjectController extends Controller
{
    protected $projectService;

    public function __construct(ProjectService $projectService)
    {
        $this->setAuthUser();
        $this->projectService = $projectService;
    }

    /**
     * Get user projects
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserProjects()
    {
        $projects = $this->projectService->getUserProjects($this->authUser->id);
        return jsonSuccess($projects);
    }

    /**
     * Create project
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createProject(Request $request)
    {
        $request->validate(['name' => 'required|string|min:1']);

        $projectId = $this->projectService->addProject($request->only(['name']), $this->authUser->id);

        if ($projectId) {
            return jsonSuccessCreate(['id' => $projectId]);
        }

        return jsonServerError();
    }

}