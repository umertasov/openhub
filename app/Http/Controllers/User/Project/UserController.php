<?php

namespace App\Http\Controllers\User\Project;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Services\ProjectService;

/**
 * @codeCoverageIgnore
 */
class UserController extends Controller
{
    protected $projectService;

    public function __construct(ProjectService $projectService)
    {
        $this->setAuthUser();
        $this->projectService = $projectService;
    }

    /**
     * Get all project users
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUsers($id)
    {
        $users = $this->projectService->getProjectUsers($id);
        if ($users !== null) {
            return jsonSuccess($users);
        }
        return jsonServerError();
    }

    /**
     * Add user to project
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addUserToProject($id, Request $request)
    {
        if (!$this->projectService->isHeadProjectUser($id, $this->authUser->id)) {
            return jsonAccessError();
        }

        $request->validate([
            'user_id' => 'required|integer|exists:users,id',
            'role' => 'required|integer|in:0,1,2'
        ]);

        $insertId = $this->projectService->addUserToProject($id, $request->input('user_id'), $request->input('role'));
        if ($insertId) {
            return jsonSuccessCreate();
        }
        return jsonServerError();
    }

    /**
     * Remove user from project
     *
     * @param $projectId
     * @param $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeUserFromProject($projectId, $userId)
    {
        if (!$this->projectService->isHeadProjectUser($projectId, $this->authUser->id)) {
            return jsonAccessError();
        }

        if ($this->projectService->removeUserFromProject($projectId, $userId)) {
            return jsonSuccess();
        }
        return jsonServerError();
    }

}