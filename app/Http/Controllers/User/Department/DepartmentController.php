<?php

namespace App\Http\Controllers\User\Department;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Services\DepartmentService;
use App\Services\ProjectService;

/**
 * @codeCoverageIgnore
 */
class DepartmentController extends Controller
{
    protected $departmentService, $projectService;

    public function __construct(DepartmentService $departmentService, ProjectService $projectService)
    {
        $this->setAuthUser();
        $this->departmentService = $departmentService;
        $this->projectService = $projectService;
    }

    /**
     * Get user departments
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserDepartments()
    {
        $departments = $this->departmentService->getUserDepartments($this->authUser->id);
        return jsonSuccess($departments);
    }

    /**
     * Create new department
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $request->validate([
            'project_id' => 'required|integer',
            'name' => 'required|string'
        ]);

        $departmentPaste = $request->only(['project_id', 'name']);

        if (!$this->projectService->isHeadProjectUser($departmentPaste['project_id'], $this->authUser->id)) {
            return jsonAccessError();
        }

        $departmentId = $this->departmentService->addDepartment($departmentPaste, $this->authUser->id);
        if ($departmentId) {
            return jsonSuccessCreate(['id' => $departmentId]);
        }
        return jsonServerError();
    }

}