<?php

namespace App\Http\Controllers\User\Department;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Services\DepartmentService;

/**
 * @codeCoverageIgnore
 */
class UserController extends Controller
{
    protected $departmentService;

    public function __construct(DepartmentService $departmentService)
    {
        $this->setAuthUser();
        $this->departmentService = $departmentService;
    }

    /**
     * Get all department users
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUsers($id)
    {
        $users = $this->departmentService->getDepartmentUsers($id);
        if ($users !== null) {
            return jsonSuccess($users);
        }
        return jsonServerError();
    }

    /**
     * Add user to department
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addUserToDepartment($id, Request $request)
    {
        if (!$this->departmentService->isHeadDepartmentUser($id, $this->authUser->id)) {
            return jsonAccessError();
        }

        $request->validate([
            'user_id' => 'required|integer|exists:users,id',
            'role' => 'required|integer|in:0,1,2'
        ]);

        $insertId = $this->departmentService->addUserToDepartment($id, $request->input('user_id'), $request->input('role'));
        if ($insertId) {
            return jsonSuccessCreate();
        }
        return jsonServerError();
    }

    /**
     * Remove user from department
     *
     * @param $id
     * @param $user_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeUserFromDepartment($id, $user_id)
    {
        if (!$this->departmentService->isHeadDepartmentUser($id, $this->authUser->id)) {
            return jsonAccessError();
        }

        if ($this->departmentService->removeUserFromDepartment($id, $user_id)) {
            return jsonSuccess();
        }
        return jsonServerError();
    }
}