<?php

namespace App\Http\Controllers\User\Organization;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Services\UserService;

/**
 * @codeCoverageIgnore
 */
class UserController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->setAuthUser();
        $this->userService = $userService;
    }

    /**
     * Create a new organization employee
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createUser(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'last_name' => 'required|string',
            'first_name' => 'required|string',
            'middle_name' => 'nullable|string',
            'birthday' => 'nullable|date',
            'phone' => 'required|string',
            'password' => 'required|string|min:8|confirmed'
        ]);

        $userPaste = $request->only(['email', 'last_name', 'first_name', 'middle_name', 'phone', 'birthday', 'password']);
        $userPaste['password'] = Hash::make($userPaste['password']);
        if (isset($editUser['birthday'])) {
            $userPaste['birthday'] = date('Y.m.d', strtotime($userPaste['birthday']));
        }

        $userId = $this->userService->addOrganizationEmployeeUser($userPaste, $this->authUser->id);

        if (!empty($userId)) {
            return jsonSuccessCreate(['id' => $userId]);
        }
        return jsonServerError();
    }

}