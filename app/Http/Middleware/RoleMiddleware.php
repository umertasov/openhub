<?php

namespace App\Http\Middleware;

use Closure;
use App\Services\UserService;
use Illuminate\Support\Facades\Auth;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param $request
     * @param Closure $next
     * @param string $role
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function handle($request, Closure $next, string $role)
    {
        if ($role === 'admin') {
            $roleId = 1;
        } else {
            $roleId = 2;
        }

        $currentUser = Auth::user();
        if (app(UserService::class)->hasRole($currentUser->id, $roleId)) {
            return $next($request);
        }
        return jsonAccessError();
    }
}
