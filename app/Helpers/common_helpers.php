<?php

if (!function_exists('dateToTimestamp')) {
    function dateToTimestamp(string $value = '')
    {
        return date('Y.m.d', strtotime($value));
    }
}