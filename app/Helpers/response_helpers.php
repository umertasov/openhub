<?php

if (!function_exists('jsonSuccess')) {
    function jsonSuccess($value = [])
    {
        return response()->json([
            'status' => 'success',
            'data' => $value
        ], 200);
    }
}

if (!function_exists('jsonSuccessCreate')) {
    function jsonSuccessCreate($value = [])
    {
        return response()->json(array_merge([
            'status' => 'success'
        ], $value), 201);
    }
}

if (!function_exists('jsonAuthorizationError')) {
    function jsonAuthorizationError($value = [])
    {
        return response()->json(array_merge([
            'status' => 'error'
        ], $value), 401);
    }
}

if (!function_exists('jsonAccessError')) {
    function jsonAccessError($value = [])
    {
        return response()->json(array_merge([
            'status' => 'error'
        ], $value), 403);
    }
}

if (!function_exists('jsonNotFound')) {
    function jsonNotFound($value = [])
    {
        return response()->json(array_merge([
            'status' => 'error'
        ], $value), 404);
    }
}

if (!function_exists('jsonValidationError')) {
    function jsonValidationError($value = [])
    {
        return response()->json(array_merge([
            'status' => 'error'
        ], $value), 422);
    }
}

if (!function_exists('jsonServerError')) {
    function jsonServerError()
    {
        return response()->json(['status' => 'error'], 500);
    }
}