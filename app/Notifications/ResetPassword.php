<?php

namespace App\Notifications;

use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;
use App\Mail\ResetPassword as Mailable;

class ResetPassword extends Notification
{
    /**
     * The password reset token.
     *
     * @var string
     */
    public $token;

    /**
     * Create a notification instance.
     *
     * @param  string  $token
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's channels.
     *
     * @param  mixed  $notifiable
     * @return array|string
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param $notifiable
     * @return $this
     */
    public function toMail($notifiable)
    {
        //$subject = 'Сброс пароля на OpenHub';
        //return (new Mailable($this->token, $notifiable))->subject($subject)->to($notifiable->email);

        return (new MailMessage)
            ->subject('Сброс пароля на OpenHub')
            ->line('Вы получили данное письмо, потому что Вы запросили сброс пароля для учетной записи OpenHub')
            ->action('Сбросить пароль', config('app.frontend_url') . '/recovery/confirm/' . $this->token . '?email=' . $notifiable->email)
            ->line('Если Вы не делали данный запрос, просто проигнорируйте это письмо.');
    }
}
