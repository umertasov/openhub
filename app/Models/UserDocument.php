<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserDocument extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['passport_series', 'passport_number', 'passport_place', 'passport_date', 'inn', 'snils'];
}
