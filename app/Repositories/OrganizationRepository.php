<?php

namespace App\Repositories;

use DB;

class OrganizationRepository extends BaseRepository
{
    public function table() : string
    {
        return 'organizations';
    }

    public function getFirstOrganizationUser(int $userId)
    {
        $organizations = DB::select("
            SELECT 
                `organizations`.`id`,
                `organizations`.`name`  
            FROM `organizations_users` 
            LEFT JOIN `organizations` ON `organizations`.`id` = `organizations_users`.`organization_id` 
            WHERE `organizations_users`.`role` = ? AND `organizations_users`.`user_id` = ? 
            LIMIT 1 
        ", [config('role.head'), $userId]);
        if (!empty($organizations)) {
            return $organizations[0];
        }
        return false;
    }
}