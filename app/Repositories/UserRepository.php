<?php

namespace App\Repositories;

use DB;

class UserRepository extends BaseRepository
{
    public function table() : string
    {
        return 'users';
    }

    public function hasRole(int $userId, int $roleId) : bool
    {
        $user = DB::select("
            SELECT 
                `users`.`id` 
            FROM `users` 
            LEFT JOIN `roles_users` ON `roles_users`.`user_id` = `users`.`id` AND `roles_users`.`role_id` = ? 
            WHERE `users`.`id` = ? AND `roles_users`.`user_id` IS NOT NULL 
        ", [$roleId, $userId]);
        if (!empty($user)) {
            return true;
        }
        return false;
    }

    public function getAvatar(int $userId)
    {
        $userInfo = DB::select("
            SELECT 
                `users`.`id`,
                `users`.`file_id`,
                `files`.`path`,
                `files`.`thumb`
            FROM `users` 
            LEFT JOIN `files` ON `files`.`id` = `users`.`file_id`
            WHERE `users`.`id` = ? 
        ", [$userId]);
        if (!empty($userInfo)) {
            return $userInfo[0];
        }
        return null;
    }
}