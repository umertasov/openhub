<?php

namespace App\Repositories;

use DB;

class RoleRepository extends BaseRepository
{
    public function table() : string
    {
        return 'roles';
    }

}