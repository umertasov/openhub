<?php

namespace App\Repositories;

use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;

abstract class BaseRepository
{
    protected $objects, $primaryKey = 'id', $attributes, $where = [], $whereIn = [], $whereNull = [], $select = [], $order = [];

    public function __construct()
    {
        $this->objects = collect();
    }

    abstract public function table() : string;

    /**
     * Create query
     *
     * @return Builder
     */
    private function query() : Builder
    {
        $query = app('db')->table($this->table());

        if (!empty($this->select)) {
            $query = $query->select($this->select);
        }

        $query = $this->handleConditions($query);

        if ($this->order) {
            $query = $query->orderBy($this->order[0], $this->order[1]);
        }

        $this->clear();

        return $query;
    }

    /**
     * Return records from the $this->objects
     *
     * @return Collection|null
     */
    public function get() :? Collection
    {
        return $this->query()->get();
    }

    /**
     * Return a single StdClass object or null
     *
     * @return object|null
     */
    public function first()
    {
        return $this->query()->first();
    }

    /**
     * Count objects
     *
     * @return int
     */
    public function count() : int
    {
        return $this->query()->count();
    }

    /**
     * Sum column values
     *
     * @param string $column
     * @return mixed
     */
    public function sum(string $column)
    {
        return $this->query()->sum($column);
    }

    /**
     * Max column value
     *
     * @param string $column
     * @return mixed
     */
    public function max(string $column)
    {
        return $this->query()->max($column);
    }

    /**
     * Min column value
     *
     * @param string $column
     * @return mixed
     */
    public function min(string $column)
    {
        return $this->query()->max($column);
    }

    /**
     * Average column value
     *
     * @param string $column
     * @return mixed
     */
    public function avg(string $column)
    {
        return $this->query()->avg($column);
    }

    /**
     * Save order parameters
     *
     * @param string $attribute
     * @param string $direction
     * @return BaseRepository
     */
    public function orderBy(string $attribute, string $direction = 'asc') : self
    {
        $this->order = [$attribute, $direction];
        return $this;
    }

    /**
     * Save a custom select clause
     *
     * @param array $fields
     * @return BaseRepository
     */
    public function select(array $fields) : self
    {
        $this->select = $fields;
        return $this;
    }

    /**
     * Insert a record and then retrieve the ID
     *
     * @param array $values
     * @return int|bool
     */
    public function insert(array $values)
    {
        if (!empty($values)) {
            $this->clear();
            return app('db')->table($this->table())->insertGetId($values);
        }
        return false;
    }

    /**
     * Insert several records into the table
     *
     * @param array $values
     * @return bool
     */
    public function insertMultiple(array $values) : bool
    {
        if (!empty($values)) {
            $values = array_filter($values, function ($item) {
                return (\is_array($item) && !empty($item));
            });

            $this->clear();
            return app('db')->table($this->table())->insert($values);
        }
        return false;
    }

    /**
     * Update record
     *
     * @param array $params
     * @return mixed
     */
    public function update(array $params = [])
    {
        $query = app('db')->table($this->table());

        $query = $this->handleConditions($query);

        $done = $query->update($params);

        $this->clear();

        return $done;
    }

    /**
     * Delete records from the table
     *
     * @return mixed
     */
    public function delete()
    {
        $query = app('db')->table($this->table());

        $query = $this->handleConditions($query);

        $done = $query->delete();

        $this->clear();

        return $done;
    }

    /**
     * Apply all conditions
     *
     * @param Builder $query
     * @return $this|Builder
     */
    protected function handleConditions(Builder $query)
    {
        if ($this->where) {
            $query = $query->where($this->where);
        }
        if ($this->whereIn) {
            foreach ($this->whereIn as $whereInParam) {
                $query = $query->whereIn($whereInParam[0], $whereInParam[1], $whereInParam[2], $whereInParam[3]);
            }
        }
        if ($this->whereNull) {
            foreach ($this->whereNull as $whereNullParam) {
                $query = $query->whereNull($whereNullParam[0], $whereNullParam[1], $whereNullParam[2]);
            }
        }

        return $query;
    }

    /*
     * ===========================================
     */


















    /*
     * ===========================================
     */


    /**
     * Add 'where in' conditions to the next queries
     *
     * @param mixed $column May be an array or array of arrays or just title of a column
     * @param array $values Array of possible values
     * @param string $operator Operator
     * @param bool $not If you want it to be "where NOT in" pass this param as true
     * @return $this
     */
    public function whereIn($column, array $values = [], string $operator = 'and', bool $not = false) : self
    {
        if (\is_array($column)) {
            if (\is_array(current($column))) {
                foreach ($column as $whereInParam) {
                    $this->whereIn($whereInParam);
                }
            } else {
                $this->whereIn(current($column), array_get($column, 1), array_get($column, 2, 'and'), array_get($column, 3, false));
            }
        } else {
            $this->whereIn[] = [$column, $values, $operator, $not];
        }

        return $this;
    }

    /**
     * Add 'where' conditions to the next queries
     *
     * @param mixed $column May be an array or array of arrays or just title of a column
     * @param null $value
     * @param string $operator
     * @param string $boolean
     * @return $this
     */
    public function where($column, $value = null, string $operator = '=', string $boolean = 'and') : self
    {
        if (\is_array($column)) {
            if (\is_array(current($column))) {
                foreach ($column as $whereColumn) {
                    $this->where($whereColumn);
                }
            } else {
                $this->where(current($column), array_get($column, 1), array_get($column, 2, '='), array_get($column, 3, 'and'));
            }
        } else {
            $this->where[] = [$column, $operator, $value, $boolean];
        }

        return $this;
    }

    /**
     * Add 'is null' conditions to the next queries
     *
     * @param mixed $column May be an array or array of arrays or just title of a column
     * @param bool $not If you want it to be 'is NOT null' pass this param as true
     * @param string $operation
     * @return $this
     */
    public function whereNull($column, bool $not = false, $operation = 'and') : self
    {
        if (\is_array($column)) {
            if (\is_array(current($column))) {
                foreach ($column as $whereNullParam) {
                    $this->whereNull($whereNullParam);
                }
            } else {
                $this->whereNull(current($column), array_get($column, 1, false), array_get($column, 2, 'and'));
            }
        } else {
            $this->whereNull[] = [$column, $operation, $not];
        }

        return $this;
    }

    /**
     * Add some objects to the $this->objects
     * @param null $collection You can pass a Collection here and it's values will be simply added to
     *                         $this->objects or you can pass an array of conditions that will be
     *                         used for the next query
     * @return $this
     */
    public function add($collection = null)
    {
        if (!($collection instanceof Collection)) {
            if (\is_array($collection)) {
                $collection = $this->runQuery(array_get($collection, 'relationships', []),
                    array_get($collection, 'limit', 0),
                    array_get($collection, 'offset', 0));
            } else {
                $collection = $this->runQuery();
            }
        }

        if ($collection->isNotEmpty()) {
            $collection->each(function ($resource) {
                if (!\is_array($resource)) $resource = (array)$resource;
                $this->objects->push($resource);
            });
        }

        return $this;
    }

    /**
     * Return a Collection of resources that fits params in $this->where, $this->where_in, $this->where_null
     * @param array $relationships
     * @param int $limit
     * @param int $offset
     * @return Collection
     */
    private function runQuery(array $relationships = [], $limit = 0, $offset = 0)
    {
        $resources = collect();

        // Создаем основу запроса, обрабатываем отношения
        $select = $this->getSelectList();
        $query = app('db')->table($this->table());

        if ($relationships) {
            $this->handleRelationships($relationships, $query, $select);
        }

        // Обрабатываем скопившиеся условия запроса
        $this->handleConditions($query);

        // Если есть заданные limit и offset, обрабатываем их
        if ($limit) {
            $query->limit($limit);
        }
        if ($offset) {
            $query->offset($offset);
        }

        $query->select($select)->get()->each(function ($queryResult) use ($resources) {
            $resources->push((array)$queryResult);
        });

        return $resources;
    }

    /**
     * Put all resources into $this->objects
     * @param array $relationships
     * @return $this
     */
    public function all(array $relationships = [])
    {
        $this->clearConditions()->clearObjects();
        return $this->add(['relationships' => $relationships]);
    }

    /**
     * Put single resource that fits passed query into $this->objects
     * @param array|integer $query                          It assumes that you pass primary key value alone or
     *                                                      an array like that ['user_id' => 5, 'status' => 1]
     * @param array $relationships
     * @return $this
     */
    public function single($query, array $relationships = [])
    {
        if (!is_array($query)) {
            $query = [$this->table . '.' . $this->primaryKey, $query];
        } else {
            foreach ($query as $key => $value) {
                $query[] = [$this->table . '.' . $key, $value];
                unset($query[$key]);
            }
        }

        $this->where($query);

        return $this->add(['relationships' => $relationships]);
    }

    public function random(int $resources_count = 6, array $exclude = [], array $relationships = [])
    {
        $all_resources_ids = app('db')->table($this->table)->select($this->primaryKey)->whereNotIn($this->primaryKey, $exclude)
            ->get()->pluck($this->primaryKey)->all();
        $resources_ids = [];
        if (count($all_resources_ids) > $resources_count) {
            $random_keys = array_rand($all_resources_ids, $resources_count);
            foreach ($random_keys as $random_key) {
                $resources_ids[] = $all_resources_ids[$random_key];
            }
        } else {
            $resources_ids = $all_resources_ids;
        }

        return $this->whereIn($this->table . '.' . $this->primaryKey, $resources_ids)->add(['relationships' => $relationships]);
    }

    /**
     * If you already have loaded resource you can add it to $this->objects
     * @param $resource
     * @return $this
     */
    public function set($resource)
    {
        $this->objects->push($resource);
        return $this;
    }

    public function setMultiple(Collection $collection)
    {
        $this->objects = $collection;
        return $this;
    }

    /**
     * Set $this->attributes with the passed array
     * @param array $attributes
     * @return $this
     */
    public function setAttributes(array $attributes = [])
    {
        if (isset($attributes['name'])) {
            $attributes['name'] = strip_tags($attributes['name']);
        }
        if (isset($attributes['title'])) {
            $attributes['title'] = strip_tags($attributes['title']);
        }
        $this->attributes = $attributes;
        return $this;
    }

    /**
     * It may be extended by a child repository to handle relationships if needed.
     * @param array $relationships
     * @param Builder $query
     * @param array $select
     */
    protected function handleRelationships(array $relationships = [], Builder $query, array &$select = [])
    {
        //
    }

    /**
     * Refresh $this->objects
     * @param bool $clear_conditions
     * @return $this
     */
    public function clear(bool $clear_conditions = true)
    {
        $this->attributes = [];
        $this->select = [];

        $this->clearObjects();

        if ($clear_conditions) $this->clearConditions();

        return $this;
    }

    public function clearConditions()
    {
        $this->where = [];
        $this->where_in = [];
        $this->where_null = [];

        return $this;
    }

    public function clearObjects()
    {
        $this->objects = collect();

        return $this;
    }

    public function getModelFillable()
    {
        $fillable = [];

        /*if ($this->model) {
            $empty_resource = new $this->model;
            $fillable = $empty_resource->getFillable();
        }*/

        return $fillable;
    }

    protected function changeQueryParams(array &$params = [])
    {
        //
    }

    protected function getSelectList(): array
    {
        return ($this->select) ? $this->select : [$this->table() . '.*'];
    }
}