<?php

namespace App\Repositories;

class FileRepository extends BaseRepository
{
    public function table() : string
    {
        return 'files';
    }
}