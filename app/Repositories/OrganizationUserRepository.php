<?php

namespace App\Repositories;

use DB;

class OrganizationUserRepository extends BaseRepository
{
    public function table() : string
    {
        return 'organizations_users';
    }
}