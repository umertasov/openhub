<?php

namespace App\Repositories;

class UserDocumentRepository extends BaseRepository
{
    public function table() : string
    {
        return 'users_documents';
    }
}