<?php

namespace App\Repositories;

use DB;

class DepartmentUserRepository extends BaseRepository
{
    public function table() : string
    {
        return 'departments_users';
    }
}