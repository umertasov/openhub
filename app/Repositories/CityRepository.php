<?php

namespace App\Repositories;

class CityRepository extends BaseRepository
{
    public function table() : string
    {
        return 'cities';
    }
}