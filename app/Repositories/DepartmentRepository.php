<?php

namespace App\Repositories;

use DB;

class DepartmentRepository extends BaseRepository
{
    public function table() : string
    {
        return 'departments';
    }

    public function getUserDepartments(int $userId)
    {
        return DB::select("
            SELECT 
                `departments`.`id`, 
                `departments`.`project_id`, 
                `departments`.`name` 
            FROM `departments_users` 
            LEFT JOIN `departments` ON `departments`.`id` = `departments_users`.`department_id` 
            WHERE `departments_users`.`user_id` = ? 
        ", [$userId]);
    }

    public function isHeadDepartmentUser(int $departmentId, int $userId) : bool
    {
        $departmentUser = DB::select("
            SELECT 
                `departments_users`.`id` 
            FROM `departments_users` 
            WHERE `departments_users`.`user_id` = ? AND `departments_users`.`department_id` = ? AND `departments_users`.`role` = ? 
        ", [$userId, $departmentId, config('role.head')]);
        if (!empty($departmentUser)) {
            return true;
        }
        return false;
    }
}