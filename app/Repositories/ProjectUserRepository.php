<?php

namespace App\Repositories;

use DB;

class ProjectUserRepository extends BaseRepository
{
    public function table() : string
    {
        return 'projects_users';
    }

}