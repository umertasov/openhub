<?php

namespace App\Repositories;

use DB;

class RoleUserRepository extends BaseRepository
{
    public function table() : string
    {
        return 'roles_users';
    }

}