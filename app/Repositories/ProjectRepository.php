<?php

namespace App\Repositories;

use DB;

class ProjectRepository extends BaseRepository
{
    public function table() : string
    {
        return 'projects';
    }

    public function getUserProjects(int $userId)
    {
        return DB::select("
            SELECT 
                `projects`.`id`, 
                `projects`.`organization_id`, 
                `projects`.`name` 
            FROM `projects_users` 
            LEFT JOIN `projects` ON `projects`.`id` = `projects_users`.`project_id` 
            WHERE `projects_users`.`user_id` = ? 
        ", [$userId]);
    }

    public function isHeadProjectUser(int $projectId, int $userId) : bool
    {
        $projectUser = DB::select("
            SELECT 
                `projects_users`.`id` 
            FROM `projects_users` 
            WHERE `projects_users`.`user_id` = ? AND `projects_users`.`project_id` = ? AND `projects_users`.`role` = ? 
        ", [$userId, $projectId, config('role.head')]);
        if (!empty($projectUser)) {
            return true;
        }
        return false;
    }

}