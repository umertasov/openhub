<?php

namespace App\Services;

use DB;

use App\Repositories\OrganizationRepository;
use App\Repositories\OrganizationUserRepository;

class OrganizationService extends Service
{
    private $organizationUserRepository;

    public function __construct(OrganizationRepository $organizationRepository, OrganizationUserRepository $organizationUserRepository)
    {
        $this->repository = $organizationRepository;
        $this->organizationUserRepository = $organizationUserRepository;
    }

    /**
     * Get full info about first organization
     *
     * @param int $userId
     * @return bool
     */
    public function getFirstOrganizationUser(int $userId)
    {
        $organization = $this->repository->getFirstOrganizationUser($userId);
        if (!empty($organization)) {
            return $organization;
        }
        return false;
    }

    /**
     * Get first organization id
     *
     * @param int $userId
     * @return bool
     */
    public function getFirstOrganizationsIdByUser(int $userId)
    {
        $organization = $this->organizationUserRepository
            ->where('user_id', $userId)
            ->where('role', config('role.head'))
            ->select(['organization_id'])
            ->first();
        if (!empty($organization)) {
            return $organization->organization_id;
        }
        return false;
    }

    /**
     * Check user role in organization
     *
     * @param $organizationId
     * @param $userId
     * @param $role
     * @return bool
     */
    public function checkUserRole($organizationId, $userId, $role) : bool
    {
        $organization = $this->organizationUserRepository
            ->where('organization_id', $organizationId)
            ->where('user_id', $userId)
            ->where('role', $role)
            ->select(['id'])
            ->first();
        if (!empty($organization)) {
            return true;
        }
        return false;
    }
}