<?php

namespace App\Services;

abstract class Service
{
    protected $repository;

    public function getList()
    {
        return $this->repository->get();
    }

    public function find(int $id)
    {
        return $this->repository->where('id', $id)->first();
    }

    public function add(array $values)
    {
        return $this->repository->insert($values);
    }

    public function edit(int $id, array $values)
    {
        return $this->repository->where('id', $id)->update($values);
    }
}