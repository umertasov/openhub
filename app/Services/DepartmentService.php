<?php

namespace App\Services;

use DB;

use App\Repositories\DepartmentRepository;
use App\Repositories\DepartmentUserRepository;

class DepartmentService extends Service
{
    private $departmentUserRepository;

    public function __construct(DepartmentRepository $departmentRepository, DepartmentUserRepository $departmentUserRepository)
    {
        $this->repository = $departmentRepository;
        $this->departmentUserRepository = $departmentUserRepository;
    }

    /**
     * Get user departments
     *
     * @param int $userId
     * @return mixed
     */
    public function getUserDepartments(int $userId)
    {
        return $this->repository->getUserDepartments($userId);
    }

    /**
     * Add department
     *
     * @param array $values
     * @param int $userId
     * @return bool|int
     */
    public function addDepartment(array $values, int $userId)
    {
        DB::beginTransaction();

        try {
            $departmentId = $this->repository->insert($values);

            $this->addUserToDepartment($departmentId, $userId, config('role.head'));

            DB::commit();

            return $departmentId;
        } catch (\Exception $e) {
            DB::rollback();
            return false;
        }
    }

    /**
     * Add user to department
     *
     * @param $departmentId
     * @param $userId
     * @param $role
     * @return bool|int
     */
    public function addUserToDepartment(int $departmentId, int $userId, int $role)
    {
        return $this->departmentUserRepository->insert([
            'user_id' => $userId,
            'department_id' => $departmentId,
            'role' => $role
        ]);
    }

    public function removeUserFromDepartment(int $departmentId, int $userId)
    {
        return $this->departmentUserRepository
            ->where('department_id', $departmentId)
            ->where('user_id', $userId)
            ->delete();
    }

    /**
     * Get all users in department
     *
     * @param $departmentId
     * @return \Illuminate\Support\Collection|null
     */
    public function getDepartmentUsers(int $departmentId)
    {
        return $this->departmentUserRepository
            ->where('department_id', $departmentId)
            ->select(['id', 'department_id', 'user_id', 'role'])
            ->get();
    }

    /**
     * Check head user in department
     *
     * @param $departmentId
     * @param $userId
     * @return bool
     */
    public function isHeadDepartmentUser(int $departmentId, int $userId) : bool
    {
        return $this->repository->isHeadDepartmentUser($departmentId, $userId);
    }
}