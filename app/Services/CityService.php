<?php

namespace App\Services;

use DB;

use App\Repositories\CityRepository;

class CityService extends Service
{
    public function __construct(CityRepository $cityRepository)
    {
        $this->repository = $cityRepository;
    }

    public function getList()
    {
        return $this->repository->select(['id', 'name', 'timezone', 'name_with_timezone'])->get();
    }
}