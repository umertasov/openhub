<?php

namespace App\Services;

use App\Repositories\ProjectUserRepository;

class ProjectUserService extends Service
{
    public function __construct(ProjectUserRepository $projectUserRepository)
    {
        $this->repository = $projectUserRepository;
    }
}