<?php

namespace App\Services;

use App\Repositories\UserDocumentRepository;

class UserDocumentService extends Service
{
    public function __construct(UserDocumentRepository $userDocumentRepository)
    {
        $this->repository = $userDocumentRepository;
    }

    /**
     * Get documents information by user id
     *
     * @param int $userId
     * @return null|object
     */
    public function getByUserId(int $userId)
    {
        $documentsInfo = $this->repository
            ->select(['passport_series', 'passport_number', 'passport_place', 'passport_date', 'inn', 'snils'])
            ->where('user_id', $userId)
            ->first();

        if (!empty($documentsInfo)) {
            return $documentsInfo;
        }

        $this->repository->insert(['user_id' => $userId]);

        return $this->repository
            ->select(['passport_series', 'passport_number', 'passport_place', 'passport_date', 'inn', 'snils'])
            ->where('user_id', $userId)
            ->first();
    }

    /**
     * Edit user documents info
     *
     * @param int $userId
     * @param array $values
     * @return bool
     */
    public function editUserDocumentsInfo(int $userId, array $values = [])
    {
        try {
            $this->repository->where('user_id', $userId)->update($values);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

}