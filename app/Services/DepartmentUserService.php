<?php

namespace App\Services;

use App\Repositories\DepartmentUserRepository;

class DepartmentUserService extends Service
{
    public function __construct(DepartmentUserRepository $departmentUserRepository)
    {
        $this->repository = $departmentUserRepository;
    }
}