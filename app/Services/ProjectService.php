<?php

namespace App\Services;

use DB;

use App\Repositories\ProjectRepository;
use App\Repositories\ProjectUserRepository;

class ProjectService extends Service
{
    private $projectUserRepository;

    public function __construct(ProjectRepository $projectRepository, ProjectUserRepository $projectUserRepository)
    {
        $this->repository = $projectRepository;
        $this->projectUserRepository = $projectUserRepository;
    }

    public function getUserProjects(int $userId)
    {
        return $this->repository->getUserProjects($userId);
    }

    /**
     * Create new project
     *
     * @param array $values
     * @param int $userId
     * @return bool|int
     */
    public function addProject(array $values, int $userId)
    {
        DB::beginTransaction();

        try {
            $organizationId = app(OrganizationService::class)->getFirstOrganizationsIdByUser($userId);
            if (!empty($organizationId)) {
                $projectId = $this->repository->insert(array_merge($values, [
                    'organization_id' => $organizationId
                ]));

                $this->addUserToProject($projectId, $userId, config('role.head'));

                DB::commit();

                return $projectId;
            }
            DB::rollback();
            return false;
        } catch (\Exception $e) {
            DB::rollback();
            return false;
        }
    }

    /**
     * Get project users
     *
     * @param int $projectId
     * @return \Illuminate\Support\Collection|null
     */
    public function getProjectUsers(int $projectId)
    {
        return $this->projectUserRepository
            ->where('project_id', $projectId)
            ->select(['id', 'project_id', 'user_id', 'role'])
            ->get();
    }

    /**
     * Add exist user to project
     *
     * @param $projectId
     * @param $userId
     * @param $role
     * @return bool|int
     */
    public function addUserToProject(int $projectId, int $userId, int $role)
    {
        return $this->projectUserRepository->insert([
            'user_id' => $userId,
            'project_id' => $projectId,
            'role' => $role
        ]);
    }

    /**
     * Remove user from project
     *
     * @param $projectId
     * @param $userId
     * @return mixed
     */
    public function removeUserFromProject(int $projectId, int $userId)
    {
        return $this->projectUserRepository
            ->where('project_id', $projectId)
            ->where('user_id', $userId)
            ->delete();
    }

    /**
     * Check head user in project
     *
     * @param $projectId
     * @param $userId
     * @return bool
     */
    public function isHeadProjectUser(int $projectId, int $userId) : bool
    {
        return $this->repository->isHeadProjectUser($projectId, $userId);
    }

}