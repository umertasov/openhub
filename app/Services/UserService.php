<?php

namespace App\Services;

use App\Repositories\UserRepository;
use App\Traits\FileTrait;
use Illuminate\Support\Facades\Hash;
use DB;

class UserService extends Service
{
    use FileTrait;

    public function __construct(UserRepository $userRepository)
    {
        $this->repository = $userRepository;
    }

    /**
     * Create new user
     *
     * @param array $values
     * @return bool|int
     */
    public function add(array $values)
    {
        DB::beginTransaction();
        try {
            $userId = $this->repository->insert($values);

            $this->repository->where('id', $userId)->update(['username' => "user{$userId}"]);

            app(UserDocumentService::class)->add(['user_id' => $userId]);

            app(RoleUserService::class)->add(['user_id' => $userId, 'role_id' => 2]);

            DB::commit();
            return $userId;
        } catch (\Exception $e) {
            DB::rollback();
            return false;
        }
    }

    /**
     * Add head user and organization
     *
     * @param array $userInfo
     * @param array $organizationInfo
     * @return bool|int
     */
    public function addOrganizationHeadUser(array $userInfo, array $organizationInfo)
    {
        DB::beginTransaction();
        try {
            $userId = $this->repository->insert($userInfo);

            $this->repository->where('id', $userId)->update(['username' => "user{$userId}"]);

            app(UserDocumentService::class)->add(['user_id' => $userId]);

            app(RoleUserService::class)->add(['user_id' => $userId, 'role_id' => config('system_role.user')]);

            $organizationId = app(OrganizationService::class)->add($organizationInfo);

            app(OrganizationUserService::class)->add([
                'user_id' => $userId,
                'organization_id' => $organizationId,
                'role' => config('role.head')
            ]);

            DB::commit();
            return $userId;
        } catch (\Exception $e) {
            DB::rollback();
            return false;
        }
    }

    /**
     * Add employee user to organization
     *
     * @param array $userInfo
     * @return bool|int
     */
    public function addOrganizationEmployeeUser(array $userInfo, int $userHeadId)
    {
        DB::beginTransaction();
        try {
            $userId = $this->repository->insert($userInfo);

            $this->repository->where('id', $userId)->update(['username' => "user{$userId}"]);

            app(UserDocumentService::class)->add(['user_id' => $userId]);

            app(RoleUserService::class)->add(['user_id' => $userId, 'role_id' => config('system_role.user')]);

            $organizationId = app(OrganizationService::class)->getFirstOrganizationsIdByUser($userHeadId);

            app(OrganizationUserService::class)->add([
                'user_id' => $userId,
                'organization_id' => $organizationId,
                'role' => config('role.employee')
            ]);

            DB::commit();
            return $userId;
        } catch (\Exception $e) {
            DB::rollback();
            return false;
        }
    }

    /**
     * Upload user avatar
     *
     * @param null $file
     * @param int $userId
     * @return bool
     * @codeCoverageIgnore
     */
    public function uploadAvatar($file = null, int $userId) : bool
    {
        DB::beginTransaction();
        try {
            $fileId = $this->uploadFile($file, 'avatars', true);
            if (!empty($fileId)) {
                $userInfo = $this->repository->getAvatar($userId);
                if ($this->repository->where('id', $userId)->update(['file_id' => $fileId])) {
                    if (!empty($userInfo)) {
                        $this->deleteFile($userInfo->fileId);
                        if (!empty($userInfo->path)) {
                            unlink(base_path($userInfo->path));
                        }
                        if (!empty($userInfo->thumb)) {
                            unlink(base_path($userInfo->thumb));
                        }
                    }
                    DB::commit();
                    return true;
                }
            }
            DB::rollback();
            return false;
        } catch (\Exception $e) {
            DB::rollback();
            return false;
        }
    }

    /**
     * Edit security options
     *
     * @param array $values
     * @param int $userId
     * @return bool
     * @throws \Exception
     */
    public function securityChange(array $values, int $userId) : bool
    {
        $user = $this->repository->where('id', $userId)->select(['id', 'email', 'phone', 'password'])->first();

        $userUpdate = [];
        if (isset($values['password'], $values['old_password']) && !empty($values['password']) && !empty($values['old_password'])) {
            if (!Hash::check($values['old_password'], $user->password)) {
                throw new \Exception('Неверный пароль');
            }
            $userUpdate['password'] = Hash::make($values['password']);
        }
        if (isset($values['email'], $values['old_email']) && !empty($values['email']) && !empty($values['old_email'])) {
            if ($user->email != $values['old_email']) {
                throw new \Exception('Неверный email');
            }
            $userUpdate['email'] = $values['email'];
        }
        if (isset($values['phone'], $values['old_phone']) && !empty($values['phone']) && !empty($values['old_phone'])) {
            if ($user->phone != $values['old_phone']) {
                throw new \Exception('Неверный телефон');
            }
            $userUpdate['phone'] = $values['phone'];
        }

        if (!empty($userUpdate)) {
            $this->repository->where('id', $userId)->update($userUpdate);
        }

        return true;
    }

    /**
     * Check username
     *
     * @param $username
     * @param $currentUserId
     * @return bool
     */
    public function usernameIsUnique(string $username, int $currentUserId) : bool
    {
        $user = $this->repository->select(['id', 'username'])
            ->where('username', $username)
            ->where('id', $currentUserId, '!=')
            ->first();
        if (!empty($user)) {
            return false;
        }
        return true;
    }

    /**
     * Check user role
     *
     * @param int $userId
     * @param int $roleId
     * @return bool
     */
    public function hasRole(int $userId, int $roleId) : bool
    {
        return $this->repository->hasRole($userId, $roleId);
    }
}