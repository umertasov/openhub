<?php

namespace App\Services;

use App\Repositories\OrganizationUserRepository;

class OrganizationUserService extends Service
{
    public function __construct(OrganizationUserRepository $organizationUserRepository)
    {
        $this->repository = $organizationUserRepository;
    }
}