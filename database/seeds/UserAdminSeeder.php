<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id' => 1,
            'email' => 'admin@admin.ru',
            'username' => 'admin',
            'last_name' => 'Админов',
            'first_name' => 'Админ',
            'middle_name' => 'Админович',
            'phone' => '79123456789',
            'password' => bcrypt('111111111')
        ]);
        DB::table('roles_users')->insert([
            'user_id' => 1,
            'role_id' => config('system_role.admin')
        ]);
    }
}
