<?php

use Illuminate\Database\Seeder;

class CitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cities')->insert([
            [
                'name' => 'Ангарск',
                'timezone' => 8,
                'name_with_timezone' => 'Ангарск (UTC+8)',
            ],
            [
                'name' => 'Архангельск',
                'timezone' => 3,
                'name_with_timezone' => 'Архангельск (UTC+3)',
            ],
            [
                'name' => 'Астрахань',
                'timezone' => 3,
                'name_with_timezone' => 'Астрахань (UTC+3)',
            ],
            [
                'name' => 'Барнаул',
                'timezone' => 6,
                'name_with_timezone' => 'Барнаул (UTC+6)',
            ],
            [
                'name' => 'Белгород',
                'timezone' => 3,
                'name_with_timezone' => 'Белгород (UTC+3)',
            ],
            [
                'name' => 'Бийск',
                'timezone' => 6,
                'name_with_timezone' => 'Бийск (UTC+6)',
            ],
            [
                'name' => 'Благовещенск',
                'timezone' => 9,
                'name_with_timezone' => 'Благовещенск (UTC+9)',
            ],
            [
                'name' => 'Братск',
                'timezone' => 8,
                'name_with_timezone' => 'Братск (UTC+8)',
            ],
            [
                'name' => 'Брянск',
                'timezone' => 3,
                'name_with_timezone' => 'Брянск (UTC+3)',
            ],
            [
                'name' => 'Великий Новгород',
                'timezone' => 3,
                'name_with_timezone' => 'Великий Новгород (UTC+3)',
            ],
            [
                'name' => 'Владивосток',
                'timezone' => 10,
                'name_with_timezone' => 'Владивосток (UTC+10)',
            ],
            [
                'name' => 'Владикавказ',
                'timezone' => 3,
                'name_with_timezone' => 'Владикавказ (UTC+3)',
            ],
            [
                'name' => 'Владимир',
                'timezone' => 3,
                'name_with_timezone' => 'Владимир (UTC+3)',
            ],
            [
                'name' => 'Волгоград',
                'timezone' => 3,
                'name_with_timezone' => 'Волгоград (UTC+3)',
            ],
            [
                'name' => 'Вологда',
                'timezone' => 3,
                'name_with_timezone' => 'Вологда (UTC+3)',
            ],
            [
                'name' => 'Воронеж',
                'timezone' => 3,
                'name_with_timezone' => 'Воронеж (UTC+3)',
            ],
            [
                'name' => 'Грозный',
                'timezone' => 3,
                'name_with_timezone' => 'Грозный (UTC+3)',
            ],
            [
                'name' => 'Дзержинск',
                'timezone' => 3,
                'name_with_timezone' => 'Дзержинск (UTC+3)',
            ],
            [
                'name' => 'Екатеринбург',
                'timezone' => 3,
                'name_with_timezone' => 'Екатеринбург (UTC+5)',
            ],
            [
                'name' => 'Иваново',
                'timezone' => 3,
                'name_with_timezone' => 'Иваново (UTC+3)',
            ],
            [
                'name' => 'Ижевск',
                'timezone' => 3,
                'name_with_timezone' => 'Ижевск (UTC+3)',
            ],
            [
                'name' => 'Иркутск',
                'timezone' => 8,
                'name_with_timezone' => 'Иркутск (UTC+8)',
            ],
            [
                'name' => 'Йошкар-Ола',
                'timezone' => 3,
                'name_with_timezone' => 'Йошкар-Ола (UTC+3)',
            ],
            [
                'name' => 'Казань',
                'timezone' => 4,
                'name_with_timezone' => 'Казань (UTC+4)',
            ],
            [
                'name' => 'Калининград',
                'timezone' => 2,
                'name_with_timezone' => 'Калининград (UTC+2)',
            ],
            [
                'name' => 'Калуга',
                'timezone' => 3,
                'name_with_timezone' => 'Калуга (UTC+3)',
            ],
            [
                'name' => 'Кемерово',
                'timezone' => 7,
                'name_with_timezone' => 'Кемерово (UTC+7)',
            ],
            [
                'name' => 'Киров',
                'timezone' => 3,
                'name_with_timezone' => 'Киров (UTC+3)',
            ],
            [
                'name' => 'Комсомольск-на-Амуре',
                'timezone' => 10,
                'name_with_timezone' => 'Комсомольск-на-Амуре (UTC+10)',
            ],
            [
                'name' => 'Кострома',
                'timezone' => 3,
                'name_with_timezone' => 'Кострома (UTC+3)',
            ],
            [
                'name' => 'Краснодар',
                'timezone' => 3,
                'name_with_timezone' => 'Краснодар (UTC+3)',
            ],
            [
                'name' => 'Красноярск',
                'timezone' => 7,
                'name_with_timezone' => 'Красноярск (UTC+7)',
            ],
            [
                'name' => 'Курган',
                'timezone' => 5,
                'name_with_timezone' => 'Курган (UTC+5)',
            ],
            [
                'name' => 'Курск',
                'timezone' => 3,
                'name_with_timezone' => 'Курск (UTC+3)',
            ],
            [
                'name' => 'Липецк',
                'timezone' => 3,
                'name_with_timezone' => 'Липецк (UTC+3)',
            ],
            [
                'name' => 'Магнитогорск',
                'timezone' => 5,
                'name_with_timezone' => 'Магнитогорск (UTC+5)',
            ],
            [
                'name' => 'Махачкала',
                'timezone' => 3,
                'name_with_timezone' => 'Махачкала (UTC+3)',
            ],
            [
                'name' => 'Минеральные Воды',
                'timezone' => 3,
                'name_with_timezone' => 'Минеральные Воды (UTC+3)',
            ],
            [
                'name' => 'Москва',
                'timezone' => 3,
                'name_with_timezone' => 'Москва (UTC+3)',
            ],
            [
                'name' => 'Мурманск',
                'timezone' => 3,
                'name_with_timezone' => 'Мурманск (UTC+3)',
            ],
            [
                'name' => 'Набережные Челны',
                'timezone' => 3,
                'name_with_timezone' => 'Набережные Челны (UTC+3)',
            ],
            [
                'name' => 'Нальчик',
                'timezone' => 3,
                'name_with_timezone' => 'Нальчик (UTC+3)',
            ],
            [
                'name' => 'Нижневартовск',
                'timezone' => 5,
                'name_with_timezone' => 'Нижневартовск (UTC+5)',
            ],
            [
                'name' => 'Нижнекамск',
                'timezone' => 3,
                'name_with_timezone' => 'Нижнекамск (UTC+3)',
            ],
            [
                'name' => 'Нижний Новгород',
                'timezone' => 3,
                'name_with_timezone' => 'Нижний Новгород (UTC+3)',
            ],
            [
                'name' => 'Нижний Тагил',
                'timezone' => 5,
                'name_with_timezone' => 'Нижний Тагил (UTC+5)',
            ],
            [
                'name' => 'Новокузнецк',
                'timezone' => 7,
                'name_with_timezone' => 'Новокузнецк (UTC+7)',
            ],
            [
                'name' => 'Новороссийск',
                'timezone' => 3,
                'name_with_timezone' => 'Новороссийск (UTC+3)',
            ],
            [
                'name' => 'Новосибирск',
                'timezone' => 6,
                'name_with_timezone' => 'Новосибирск (UTC+6)',
            ],
            [
                'name' => 'Омск',
                'timezone' => 6,
                'name_with_timezone' => 'Омск (UTC+6)',
            ],
            [
                'name' => 'Орёл',
                'timezone' => 3,
                'name_with_timezone' => 'Орёл (UTC+3)',
            ],
            [
                'name' => 'Оренбург',
                'timezone' => 5,
                'name_with_timezone' => 'Оренбург (UTC+5)',
            ],
            [
                'name' => 'Орск',
                'timezone' => 5,
                'name_with_timezone' => 'Орск (UTC+5)',
            ],
            [
                'name' => 'Пенза',
                'timezone' => 3,
                'name_with_timezone' => 'Пенза (UTC+3)',
            ],
            [
                'name' => 'Пермь',
                'timezone' => 5,
                'name_with_timezone' => 'Пермь (UTC+5)',
            ],
            [
                'name' => 'Петрозаводск',
                'timezone' => 3,
                'name_with_timezone' => 'Петрозаводск (UTC+3)',
            ],
            [
                'name' => 'Прокопьевск',
                'timezone' => 7,
                'name_with_timezone' => 'Прокопьевск (UTC+7)',
            ],
            [
                'name' => 'Псков',
                'timezone' => 3,
                'name_with_timezone' => 'Псков (UTC+3)',
            ],
            [
                'name' => 'Ростов-на-Дону',
                'timezone' => 3,
                'name_with_timezone' => 'Ростов-на-Дону (UTC+3)',
            ],
            [
                'name' => 'Рязань',
                'timezone' => 3,
                'name_with_timezone' => 'Рязань (UTC+3)',
            ],
            [
                'name' => 'Самара',
                'timezone' => 4,
                'name_with_timezone' => 'Самара (UTC+4)',
            ],
            [
                'name' => 'Санкт-Петербург',
                'timezone' => 3,
                'name_with_timezone' => 'Санкт-Петербург (UTC+3)',
            ],
            [
                'name' => 'Саранск',
                'timezone' => 3,
                'name_with_timezone' => 'Саранск (UTC+3)',
            ],
            [
                'name' => 'Саратов',
                'timezone' => 3,
                'name_with_timezone' => 'Саратов (UTC+3)',
            ],
            [
                'name' => 'Севастополь',
                'timezone' => 3,
                'name_with_timezone' => 'Севастополь (UTC+3)',
            ],
            [
                'name' => 'Симферопль',
                'timezone' => 3,
                'name_with_timezone' => 'Симферопль (UTC+3)',
            ],
            [
                'name' => 'Смоленск',
                'timezone' => 3,
                'name_with_timezone' => 'Смоленск (UTC+3)',
            ],
            [
                'name' => 'Сочи',
                'timezone' => 3,
                'name_with_timezone' => 'Сочи (UTC+3)',
            ],
            [
                'name' => 'Ставрополь',
                'timezone' => 3,
                'name_with_timezone' => 'Ставрополь (UTC+3)',
            ],
            [
                'name' => 'Старый Оскол',
                'timezone' => 3,
                'name_with_timezone' => 'Старый Оскол (UTC+3)',
            ],
            [
                'name' => 'Стерлитамак',
                'timezone' => 5,
                'name_with_timezone' => 'Стерлитамак (UTC+5)',
            ],
            [
                'name' => 'Сургут',
                'timezone' => 5,
                'name_with_timezone' => ' (UTC+5)',
            ],
            [
                'name' => 'Сыктывкар',
                'timezone' => 3,
                'name_with_timezone' => 'Сыктывкар (UTC+3)',
            ],
            [
                'name' => 'Таганрог',
                'timezone' => 3,
                'name_with_timezone' => 'Таганрог (UTC+3)',
            ],
            [
                'name' => 'Тамбов',
                'timezone' => 3,
                'name_with_timezone' => 'Тамбов (UTC+3)',
            ],
            [
                'name' => 'Тверь',
                'timezone' => 3,
                'name_with_timezone' => 'Тверь (UTC+3)',
            ],
            [
                'name' => 'Тольятти',
                'timezone' => 4,
                'name_with_timezone' => 'Тольятти (UTC+4)',
            ],
            [
                'name' => 'Томск',
                'timezone' => 6,
                'name_with_timezone' => 'Томск (UTC+6)',
            ],
            [
                'name' => 'Тула',
                'timezone' => 3,
                'name_with_timezone' => 'Тула (UTC+3)',
            ],
            [
                'name' => 'Тюмень',
                'timezone' => 5,
                'name_with_timezone' => 'Тюмень (UTC+5)',
            ],
            [
                'name' => 'Улан-Удэ',
                'timezone' => 8,
                'name_with_timezone' => 'Улан-Удэ (UTC+8)',
            ],
            [
                'name' => 'Ульяновск',
                'timezone' => 4,
                'name_with_timezone' => 'Ульяновск (UTC+4)',
            ],
            [
                'name' => 'Уфа',
                'timezone' => 5,
                'name_with_timezone' => 'Уфа (UTC+5)',
            ],
            [
                'name' => 'Хабаровск',
                'timezone' => 10,
                'name_with_timezone' => 'Хабаровск (UTC+10)',
            ],
            [
                'name' => 'Чебоксары',
                'timezone' => 3,
                'name_with_timezone' => 'Чебоксары (UTC+3)',
            ],
            [
                'name' => 'Челябинск',
                'timezone' => 5,
                'name_with_timezone' => 'Челябинск (UTC+5)',
            ],
            [
                'name' => 'Череповец',
                'timezone' => 3,
                'name_with_timezone' => 'Череповец (UTC+3)',
            ],
            [
                'name' => 'Чита',
                'timezone' => 9,
                'name_with_timezone' => 'Чита (UTC+9)',
            ],
            [
                'name' => 'Шахты',
                'timezone' => 3,
                'name_with_timezone' => 'Шахты (UTC+3)',
            ],
            [
                'name' => 'Якутск',
                'timezone' => 9,
                'name_with_timezone' => 'Якутск (UTC+9)',
            ],
            [
                'name' => 'Ярославль',
                'timezone' => 3,
                'name_with_timezone' => 'Ярославль (UTC+3)',
            ]
        ]);
    }
}
