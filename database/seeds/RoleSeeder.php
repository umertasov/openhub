<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
                'id' => config('system_role.admin'),
                'name' => 'admin',
                'display_name' => 'Администратор',
                'description' => 'Администратор',
            ],
            [
                'id' => config('system_role.user'),
                'name' => 'user',
                'display_name' => 'Пользователь',
                'description' => 'Пользователь',
            ]
        ]);
    }
}
