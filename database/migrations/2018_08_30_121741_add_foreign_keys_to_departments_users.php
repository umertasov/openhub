<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToDepartmentsUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('departments_users', function (Blueprint $table) {
            $table->foreign('department_id', 'departments_users_department_id')->references('id')->on('departments')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('user_id', 'departments_users_user_id')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('departments_users', function (Blueprint $table) {
            $table->dropForeign('departments_users_department_id');
            $table->dropForeign('departments_users_user_id');
        });
    }
}
