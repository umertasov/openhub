<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->timestamp('birthday')->useCurrent()->after('email');
            $table->string('phone')->after('email');
            $table->string('middle_name')->after('email');
            $table->string('first_name')->after('email');
            $table->string('last_name')->after('email');
            $table->string('username')->after('email')->unique();

            $table->dropColumn('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['username', 'last_name', 'first_name', 'middle_name', 'phone', 'birthday']);

            $table->string('name')->after('id');
        });
    }
}
