<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToProjectsUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projects_users', function (Blueprint $table) {
            $table->foreign('project_id', 'projects_users_project_id')->references('id')->on('projects')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('user_id', 'projects_users_user_id')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('projects_users', function (Blueprint $table) {
            $table->dropForeign('projects_users_project_id');
            $table->dropForeign('projects_users_user_id');
        });
    }
}
