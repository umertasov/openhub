<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToOrganizationsUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('organizations_users', function (Blueprint $table) {
            $table->foreign('organization_id', 'organizations_users_organization_id')->references('id')->on('organizations')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('user_id', 'organizations_users_user_id')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('organizations_users', function (Blueprint $table) {
            $table->dropForeign('organizations_users_organization_id');
            $table->dropForeign('organizations_users_user_id');
        });
    }
}
